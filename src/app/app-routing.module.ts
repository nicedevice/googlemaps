import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'google-maps', loadChildren: './pages/google-maps/google-maps.module#GoogleMapsPageModule' },
  { path: 'google-maps-ab', loadChildren: './pages/google-maps-ab/google-maps-ab.module#GoogleMapsAbPageModule' },
  { path: 'google-maps-size', loadChildren: './pages/google-maps-size/google-maps-size.module#GoogleMapsSizePageModule' },
  { path: 'google-maps-clustering', loadChildren: './pages/google-maps-clustering/google-maps-clustering.module#GoogleMapsClusteringPageModule' },
  // { path: 'google-maps-ab-options', loadChildren: './pages/google-maps-ab-options/google-maps-ab-options.module#GoogleMapsAbOptionsPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
