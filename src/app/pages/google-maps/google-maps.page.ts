import { Component, OnInit } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMapOptions,
  GoogleMapsMapTypeId,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  Circle,
  Spherical,
  GoogleMapsAnimation,
  MyLocation,
  LatLng
} from '@ionic-native/google-maps/ngx';


@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.page.html',
  styleUrls: ['./google-maps.page.scss']
})
export class GoogleMapsPage implements OnInit {

  map: GoogleMap;

  constructor(
    private platform: Platform,
    private toastCtrl: ToastController
  ) { 
  }

  async ngOnInit() {
    await this.platform.ready()
    await this.loadMap();
  }
  

  loadMap() {
    let mapOptions: GoogleMapOptions = {
      mapType: GoogleMapsMapTypeId.SATELLITE,
    
      controls: {
        compass: true,
        myLocationButton: true,
        myLocation: true, // (blue dot)
        zoom: true,       // android only
        mapToolbar: false  // android only
      }
    }
    this.map = GoogleMaps.create('map', mapOptions);
    
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.showToast('Google Map is ready');
      console.log('Google Map is ready');
    });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2500,
      position: 'middle'
    });
    toast.present();
  }
}
