import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-google-maps-ab-options',
  templateUrl: './google-maps-ab-options.page.html',
  styleUrls: ['./google-maps-ab-options.page.scss'],
})
export class GoogleMapsAbOptionsPage implements OnInit {

  public selectedRange: string;

  constructor(
    private navParams: NavParams, 
    private popoverCtrl: PopoverController
  ) {
    
  }

  ngOnInit() {
    this.selectedRange = this.navParams.get('range');
  }

  setRange(ev) {
    this.popoverCtrl.dismiss(
      ev.detail
    )
  }

}
