import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GoogleMapsSizePage } from './google-maps-size.page';

const routes: Routes = [
  {
    path: '',
    component: GoogleMapsSizePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GoogleMapsSizePage]
})
export class GoogleMapsSizePageModule {}
