import { Component, OnInit } from '@angular/core';
import { Platform, ToastController, LoadingController, } from '@ionic/angular';

import { 
  GoogleMaps,
  GoogleMapsMapTypeId,
  GoogleMapsEvent,
  Spherical,
  GoogleMap,
  GoogleMapOptions,
  Circle,
  CircleOptions,
  Marker,
  MarkerOptions,
  MarkerCluster,
  MarkerClusterOptions,
  MarkerClusterIcon,
  GoogleMapsAnimation,
  ILatLng,
  MyLocation
} from '@ionic-native/google-maps/ngx';

const defLocation: ILatLng = {
  lat: 56.263920,
  lng: 9.501785
};

@Component({
  selector: 'app-google-maps-clustering',
  templateUrl: './google-maps-clustering.page.html',
  styleUrls: ['./google-maps-clustering.page.scss'],
})
export class GoogleMapsClusteringPage implements OnInit {

  map: GoogleMap;
  mapMarker: Marker;

  private loading: HTMLIonLoadingElement;

  constructor(
    private platform: Platform,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) { }

  async ngOnInit() {
    console.log('google-maps-clustering: ngOnInit()');

    await this.platform.ready();
    await this.loadMap();
  }

  ionViewWillEnter() {
    console.log('google-maps-clustering: ionViewWillEnter()');

    // load map and markers
    if (!this.map) {
      this.loadMap();
    }
  }

  loadMap() {
    console.log('google-maps-clustering: loadMap()');
    // Google maps API keys are set in app.component.ts

    let mapOptions: GoogleMapOptions = {
      mapType: 'MAP_TYPE_ROADMAP', //GoogleMapsMapTypeId.HYBRID,
      controls: {
        compass: true,
        myLocationButton: false,
        myLocation: false,   // (blue dot)
        indoorPicker: false,
        zoom: true,          // android only
        mapToolbar: true     // android only
      },
      gestures: {
        scroll: true,
        tilt: false,
        zoom: true,
        rotate: false
      }
    }
    this.map = GoogleMaps.create('map', mapOptions);


    this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
      console.log('google-maps-clustering: Map ready!');

      // move the map camera to the user location
      this.map.animateCamera({
        target: defLocation,
        zoom: 10,
        // duration: 10000
      });
  
      // add a marker to the user location
      this.map.addMarkerSync({
        position: defLocation,
        animation: GoogleMapsAnimation.BOUNCE
      });

      let convertData = this.dummyData().map(a => {
        return {          
          title: a.name,
          position: {
            lat: a.lat,
            lng: a.lng
          },
          icon: 'blue',
          draggable: false
        }

      });
      this.addClusterMarkers(convertData);

    });
  };
      
  addClusterMarkers(data) {
    console.log('addCluserMarkers(): ', data)
    this.map.addMarkerCluster({
      boundsDraw: true,
      maxZoomLevel: 12,
      markers: data,
      icons: [
        {
          min: 3,
          max: 15,
          // url: 'https://www.jagtlog.com/images/apps/small.png',
          url: './assets/image/small.png',
          label: {
            color: 'white'
          }
        },
        {
          min: 16,
          max: 250,
          // url: 'https://www.jagtlog.com/images/apps/large.png',
          url: './assets/image/large.png',
          label: {
            color: 'white'
          }
        }
      ]
    }).then((markerCluster: MarkerCluster) => {

      markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
        console.log(params);
        // let latLng: LatLng = params[0];

        // let marker: Marker = params[1];

        // marker.setAnimation(GoogleMapsAnimation.BOUNCE);
      });

    });
  }

  dummyData() {
    return [
      {
          "id": 1,
          "name": "Ole Knudsen",
          "lat": "55.594114",
          "lng": "8.244994"
      },
      {
          "id": 2,
          "name": "Carsten Batzlaff",
          "lat": "54.874187",
          "lng": "9.313094"
      },
      {
          "id": 3,
          "name": "Hans Peter Abkjer Boldt",
          "lat": "54.9474717",
          "lng": "8.7972622"
      },
      {
          "id": 4,
          "name": "Jørgen Lang",
          "lat": "55.2730436",
          "lng": "11.397693"
      },
      {
          "id": 5,
          "name": "Viggo Vind",
          "lat": "55.350368",
          "lng": "11.556967"
      },
      {
          "id": 6,
          "name": "Lindy Nielsen",
          "lat": "56.1390111",
          "lng": "9.1698865"
      },
      {
          "id": 7,
          "name": "Jens Lindstrøm",
          "lat": "56.89566",
          "lng": "10.044243"
      },
      {
          "id": 8,
          "name": "Henrik S. Kristensen",
          "lat": "56.911877",
          "lng": "8.386756"
      },
      {
          "id": 9,
          "name": "Jens Jørn Kallesø Sørensen",
          "lat": "56.178691",
          "lng": "8.317534"
      },
      {
          "id": 10,
          "name": "Bent Bolli Hansen",
          "lat": "57.57597",
          "lng": "9.943875"
      },
      {
          "id": 11,
          "name": "John Pedersen",
          "lat": "56.1167072",
          "lng": "8.1179581"
      },
      {
          "id": 12,
          "name": "Jørn Lund-Thomsen",
          "lat": "56.739774",
          "lng": "9.484196"
      },
      {
          "id": 13,
          "name": "Karl Andersen",
          "lat": "55.775072",
          "lng": "8.288002"
      },
      {
          "id": 14,
          "name": "Kristian Graversen",
          "lat": "55.569407",
          "lng": "12.60853"
      },
      {
          "id": 15,
          "schweisid": 133,
          "name": "Jesper Grønlund",
          "schweiss_address": "Fussingøvej 19",
          "schweiss_postal": "8920",
          "schweiss_city": "Randers NV",
          "schweiss_region": "Randers",
          "schweiss_phone": "",
          "schweiss_cell": "21488349",
          "schweiss_email": "jespergronlund@icloud.com",
          "ref_schweiss_region_id": 2,
          "lat": "56.4619953",
          "lng": "9.8430966"
      },
      {
          "id": 16,
          "schweisid": 156,
          "name": "Jens Erik K. Bro",
          "schweiss_address": "Gl. Landevej 26",
          "schweiss_postal": "7620",
          "schweiss_city": "Lemvig",
          "schweiss_region": "Lemvig",
          "schweiss_phone": "",
          "schweiss_cell": "20136776",
          "schweiss_email": "brojenserik@gmail.com",
          "ref_schweiss_region_id": 3,
          "lat": "56.506443",
          "lng": "8.322592"
      },
      {
          "id": 18,
          "schweisid": 174,
          "name": "Gunnar Sørensen",
          "schweiss_address": "Lunde Mark 44",
          "schweiss_postal": "9460",
          "schweiss_city": "Brovst",
          "schweiss_region": "Jammerbugt",
          "schweiss_phone": "98235251",
          "schweiss_cell": "22434251",
          "schweiss_email": "bettelunde@meijers.dk",
          "ref_schweiss_region_id": 1,
          "lat": "57.1191893",
          "lng": "9.4245649"
      },
      {
          "id": 19,
          "schweisid": 178,
          "name": "Niels Christoffersen",
          "schweiss_address": "Viaduktvej 6",
          "schweiss_postal": "4750",
          "schweiss_city": "Lundby",
          "schweiss_region": "Næstved",
          "schweiss_phone": "",
          "schweiss_cell": "51214482",
          "schweiss_email": "hundeniels@gmail.com",
          "ref_schweiss_region_id": 8,
          "lat": "55.1337184",
          "lng": "11.8509091"
      },
      {
          "id": 20,
          "schweisid": 200,
          "name": "Laurids Jonassen",
          "schweiss_address": "Østergade 28",
          "schweiss_postal": "7361",
          "schweiss_city": "Ejstrupholm",
          "schweiss_region": "Ikast-Brande",
          "schweiss_phone": "75772223",
          "schweiss_cell": "40626611",
          "schweiss_email": "lj@mvbmail.dk",
          "ref_schweiss_region_id": 3,
          "lat": "55.983089",
          "lng": "9.294123"
      },
      {
          "id": 21,
          "schweisid": 204,
          "name": "Frits Hansen",
          "schweiss_address": "Engtoften 2",
          "schweiss_postal": "8586",
          "schweiss_city": "Ørum Djurs",
          "schweiss_region": "Norddjurs",
          "schweiss_phone": "86386368",
          "schweiss_cell": "21278041",
          "schweiss_email": "frits_hansen@live.dk",
          "ref_schweiss_region_id": 2,
          "lat": "56.4438303",
          "lng": "10.6738479"
      },
      {
          "id": 22,
          "schweisid": 228,
          "name": "Jørn Kærsgaard",
          "schweiss_address": "Gudenåvej 23",
          "schweiss_postal": "8643",
          "schweiss_city": "Ans By",
          "schweiss_region": "Silkeborg",
          "schweiss_phone": "86870223",
          "schweiss_cell": "40512489",
          "schweiss_email": "jkab8643@gmail.com",
          "ref_schweiss_region_id": 4,
          "lat": "56.306145",
          "lng": "9.675148"
      },
      {
          "id": 23,
          "schweisid": 232,
          "name": "Kim G. Pedersen",
          "schweiss_address": "Olstrupvej 10",
          "schweiss_postal": "4690",
          "schweiss_city": "Haslev",
          "schweiss_region": "Faxe",
          "schweiss_phone": "",
          "schweiss_cell": "20260931",
          "schweiss_email": "kimgronholt@gmail.com",
          "ref_schweiss_region_id": 8,
          "lat": "55.2878812",
          "lng": "12.0347792"
      },
      {
          "id": 24,
          "schweisid": 239,
          "name": "Vagn Rasmussen",
          "schweiss_address": "Ladegårdvej 37",
          "schweiss_postal": "6100",
          "schweiss_city": "Haderslev",
          "schweiss_region": "Haderslev",
          "schweiss_phone": "74527370",
          "schweiss_cell": "30746433",
          "schweiss_email": "sigrid-vagn@privat.dk",
          "ref_schweiss_region_id": 5,
          "lat": "55.271613",
          "lng": "9.5214839"
      },
      {
          "id": 25,
          "schweisid": 240,
          "name": "Preben Røge",
          "schweiss_address": "Thorup Strandvej 78",
          "schweiss_postal": "9690",
          "schweiss_city": "Fjerritslev",
          "schweiss_region": "Jammerbugt",
          "schweiss_phone": "98225668",
          "schweiss_cell": "26875452",
          "schweiss_email": "prebenroge@gmail.com",
          "ref_schweiss_region_id": 1,
          "lat": "57.108875",
          "lng": "9.137143"
      },
      {
          "id": 26,
          "name": "Poul-Erik Hansen",
          "lat": "56.485631",
          "lng": "8.585244"
      },
      {
          "id": 27,
          "name": "Stig Jensen",
          "lat": "56.017671",
          "lng": "12.3669119"
      },
      {
          "id": 28,
          "name": "Kristian Mortensen",
          "lat": "56.204052",
          "lng": "8.313666"
      },
      {
          "id": 29,
          "name": "Lars Haugaard",
          "lat": "56.195177",
          "lng": "10.442796"
      },
      {
          "id": 30,
          "name": "Jørgen Ibsen",
          "lat": "55.48436",
          "lng": "9.744863"
      },
      {
          "id": 31,
          "name": "Bo Christensen",
          "lat": "55.0309836",
          "lng": "11.8843395"
      },
      {
          "id": 32,
          "schweisid": 274,
          "name": "Jørgen Jensen",
          "schweiss_address": "Lilleheden Skovvej 10",
          "schweiss_postal": "9850",
          "schweiss_city": "Hirtshals",
          "schweiss_region": "Hjørring",
          "schweiss_phone": "",
          "schweiss_cell": "21618355",
          "schweiss_email": "jjens@nst.dk",
          "ref_schweiss_region_id": 1,
          "lat": "57.582813",
          "lng": "9.989895"
      },
      {
          "id": 33,
          "name": "Finn K. Meier",
          "lat": "56.4721686",
          "lng": "8.9698482"
      },
      {
          "id": 34,
          "name": "Søren Jørgensen",
          "lat": "55.841802",
          "lng": "10.5678889"
      },
      {
          "id": 35,
          "name": "Villy Hjorth Sørensen",
          "lat": "55.4632998",
          "lng": "9.0244768"
      },
      {
          "id": 36,
          "name": "Keld M. Pedersen",
          "lat": "55.253355",
          "lng": "9.158163"
      },
      {
          "id": 37,
          "name": "Christian Jessen",
          "lat": "54.961371",
          "lng": "9.861023"
      },
      {
          "id": 38,
          "name": "Bent Hundstrup",
          "lat": "55.2360941",
          "lng": "10.254699"
      },
      {
          "id": 39,
          "name": "Rune Rübner-Petersen",
          "lat": "54.784664",
          "lng": "12.019249"
      },
      {
          "id": 40,
          "name": "Paul Baunkjær",
          "lat": "55.092642",
          "lng": "14.882654"
      },
      {
          "id": 41,
          "name": "Karsten B. Pedersen",
          "lat": "56.237501",
          "lng": "8.990689"
      },
      {
          "id": 42,
          "schweisid": 298,
          "name": "Bent Jørgensen",
          "schweiss_address": "Amagervænget 9",
          "schweiss_postal": "6900",
          "schweiss_city": "Skjern",
          "schweiss_region": "Ringkøbing-Skjern",
          "schweiss_phone": "97352058",
          "schweiss_cell": "20482982",
          "schweiss_email": "bjjj@skjern-net.dk",
          "ref_schweiss_region_id": 3,
          "lat": "55.9406753",
          "lng": "8.4829278"
      },
      {
          "id": 43,
          "schweisid": 300,
          "name": "Lars Chr. Jensen",
          "schweiss_address": "Grøftebjergvej 2",
          "schweiss_postal": "5610",
          "schweiss_city": "Assens",
          "schweiss_region": "Assens",
          "schweiss_phone": "64741946",
          "schweiss_cell": "40581942",
          "schweiss_email": "lars@linox.dk",
          "ref_schweiss_region_id": 6,
          "lat": "55.2475798",
          "lng": "10.020053"
      },
      {
          "id": 44,
          "schweisid": 306,
          "name": "Lars Knudsen",
          "schweiss_address": "Bygvænget 29",
          "schweiss_postal": "4900",
          "schweiss_city": "Nakskov",
          "schweiss_region": "Lolland",
          "schweiss_phone": "",
          "schweiss_cell": "23435068",
          "schweiss_email": "knudsen.lak@gmail.com",
          "ref_schweiss_region_id": 8,
          "lat": "54.8446249",
          "lng": "11.1177591"
      },
      {
          "id": 45,
          "schweisid": 307,
          "name": "Christian Kjær-Andersen",
          "schweiss_address": "Rundbjergvej 9",
          "schweiss_postal": "6535",
          "schweiss_city": "Branderup J",
          "schweiss_region": "Tønder",
          "schweiss_phone": "74835353",
          "schweiss_cell": "29386265",
          "schweiss_email": "cka@toender.dk",
          "ref_schweiss_region_id": 5,
          "lat": "55.135583",
          "lng": "9.041094"
      },
      {
          "id": 46,
          "schweisid": 308,
          "name": "Poul Erik Kristensen",
          "schweiss_address": "Nebbegårdsvej 127",
          "schweiss_postal": "7000",
          "schweiss_city": "Fredericia",
          "schweiss_region": "Vejle",
          "schweiss_phone": "",
          "schweiss_cell": "24416687",
          "schweiss_email": "kristensenpe@gmail.com",
          "ref_schweiss_region_id": 4,
          "lat": "55.617075",
          "lng": "9.703078"
      },
      {
          "id": 47,
          "schweisid": 309,
          "name": "Hans H. Henningsen",
          "schweiss_address": "Vadstedvej 18",
          "schweiss_postal": "6560",
          "schweiss_city": "Sommersted",
          "schweiss_region": "Haderslev",
          "schweiss_phone": "",
          "schweiss_cell": "40431144",
          "schweiss_email": "hans@henningsenas.dk",
          "ref_schweiss_region_id": 5,
          "lat": "55.3186249",
          "lng": "9.2428689"
      },
      {
          "id": 48,
          "schweisid": 310,
          "name": "Henri Vivike",
          "schweiss_address": "Alkenvej 141",
          "schweiss_postal": "8660",
          "schweiss_city": "Skanderborg",
          "schweiss_region": "Skanderborg",
          "schweiss_phone": "86577137",
          "schweiss_cell": "40730978",
          "schweiss_email": "hannehenri@gmail.com",
          "ref_schweiss_region_id": 4,
          "lat": "56.048518",
          "lng": "9.848926"
      },
      {
          "id": 49,
          "schweisid": 314,
          "name": "Henrik Korsholm",
          "schweiss_address": "Skovgade 14",
          "schweiss_postal": "7300",
          "schweiss_city": "Jelling",
          "schweiss_region": "Vejle",
          "schweiss_phone": "",
          "schweiss_cell": "20912500",
          "schweiss_email": "hekorsholm@gmail.com",
          "ref_schweiss_region_id": 4,
          "lat": "55.75364",
          "lng": "9.4247789"
      },
      {
          "id": 50,
          "schweisid": 316,
          "name": "Lars Mikkelsen",
          "schweiss_address": "Hodsagervej 36",
          "schweiss_postal": "7490",
          "schweiss_city": "Aulum",
          "schweiss_region": "Herning",
          "schweiss_phone": "",
          "schweiss_cell": "20414930",
          "schweiss_email": "lars@jagtrammer.dk",
          "ref_schweiss_region_id": 3,
          "lat": "56.333425",
          "lng": "8.786768"
      },
      {
          "id": 51,
          "schweisid": 320,
          "name": "Per Christensen",
          "schweiss_address": "Bjerringbrovej 46",
          "schweiss_postal": "8850",
          "schweiss_city": "Bjerringbro",
          "schweiss_region": "Viborg",
          "schweiss_phone": "",
          "schweiss_cell": "21483107",
          "schweiss_email": "perc.valsgaard@mail.dk",
          "ref_schweiss_region_id": 2,
          "lat": "56.354383",
          "lng": "9.6015184"
      },
      {
          "id": 52,
          "schweisid": 321,
          "name": "Jørgen H. Gravesen",
          "schweiss_address": "Vester Skivevej 92 B",
          "schweiss_postal": "8800",
          "schweiss_city": "Viborg",
          "schweiss_region": "Viborg",
          "schweiss_phone": "97548061",
          "schweiss_cell": "40841425",
          "schweiss_email": "djgravesen@gmail.com",
          "ref_schweiss_region_id": 2,
          "lat": "56.432902",
          "lng": "9.068087"
      },
      {
          "id": 54,
          "schweisid": 329,
          "name": "Steen S. Petersen",
          "schweiss_address": "Debelvej 170",
          "schweiss_postal": "6855",
          "schweiss_city": "Outrup",
          "schweiss_region": "Varde",
          "schweiss_phone": "75251031",
          "schweiss_cell": "22433703",
          "schweiss_email": "sp@spfpels.dk",
          "ref_schweiss_region_id": 3,
          "lat": "55.724454",
          "lng": "8.434553"
      },
      {
          "id": 55,
          "schweisid": 330,
          "name": "Henning Andersen",
          "schweiss_address": "Egevej 16",
          "schweiss_postal": "4632",
          "schweiss_city": "Bjæverskov",
          "schweiss_region": "Køge",
          "schweiss_phone": "56870083",
          "schweiss_cell": "40250283",
          "schweiss_email": "solvraven@privat.dk",
          "ref_schweiss_region_id": 7,
          "lat": "55.4648728",
          "lng": "12.0648249"
      },
      {
          "id": 56,
          "schweisid": 331,
          "name": "Ole Lund",
          "schweiss_address": "Skårupgård 43",
          "schweiss_postal": "9870",
          "schweiss_city": "Sindal",
          "schweiss_region": "Hjørring",
          "schweiss_phone": "",
          "schweiss_cell": "30900945",
          "schweiss_email": "olelund51@gmail.com",
          "ref_schweiss_region_id": 1,
          "lat": "57.460727",
          "lng": "10.341167"
      },
      {
          "id": 57,
          "schweisid": 333,
          "name": "Johnny Marius Merkelsen",
          "schweiss_address": "Enghavevej 14",
          "schweiss_postal": "8450",
          "schweiss_city": "Hammel",
          "schweiss_region": "Favrskov",
          "schweiss_phone": "40303518",
          "schweiss_cell": "22594014",
          "schweiss_email": "johnnymerkelsen@live.dk",
          "ref_schweiss_region_id": 4,
          "lat": "56.2587454",
          "lng": "9.8603535"
      },
      {
          "id": 58,
          "schweisid": 334,
          "name": "Per Hyttel",
          "schweiss_address": "Fosdalvej 24",
          "schweiss_postal": "9460",
          "schweiss_city": "Brovst",
          "schweiss_region": "Jammerbugt",
          "schweiss_phone": "98235046",
          "schweiss_cell": "26875430",
          "schweiss_email": "phy@nst.dk",
          "ref_schweiss_region_id": 1,
          "lat": "57.120841",
          "lng": "9.420305"
      },
      {
          "id": 59,
          "schweisid": 339,
          "name": "Leif Stonor Nielsen",
          "schweiss_address": "Svalebækvej 1",
          "schweiss_postal": "4690",
          "schweiss_city": "Haslev",
          "schweiss_region": "Faxe",
          "schweiss_phone": "",
          "schweiss_cell": "29666381",
          "schweiss_email": "leif_stonor@nielsen.mail.dk",
          "ref_schweiss_region_id": 7,
          "lat": "55.3117636",
          "lng": "11.8758661"
      },
      {
          "id": 60,
          "schweisid": 342,
          "name": "Niels Peter Madsen",
          "schweiss_address": "Hine Møllevej 11",
          "schweiss_postal": "5900",
          "schweiss_city": "Rudkøbing",
          "schweiss_region": "Langeland",
          "schweiss_phone": "62512462",
          "schweiss_cell": "21923256",
          "schweiss_email": "n.l.madsen@hotmail.com",
          "ref_schweiss_region_id": 6,
          "lat": "54.9325637",
          "lng": "10.7302501"
      },
      {
          "id": 61,
          "name": "Morten Hovalt Bruun Aaen",
          "lat": "56.9977639",
          "lng": "10.3179877"
      },
      {
          "id": 62,
          "name": "Peter Larsen",
          "lat": "55.2323108",
          "lng": "11.7406308"
      },
      {
          "id": 63,
          "name": "Anders Laigaard",
          "lat": "56.608577",
          "lng": "9.020866"
      },
      {
          "id": 64,
          "name": "Flemming Thune-Stephensen",
          "lat": "56.2769312",
          "lng": "10.0754084"
      },
      {
          "id": 65,
          "name": "Erik Jenner",
          "lat": "56.8400354",
          "lng": "9.4219642"
      },
      {
          "id": 66,
          "name": "Jens Chr. Stræde",
          "lat": "56.0841377",
          "lng": "8.2808026"
      },
      {
          "id": 67,
          "name": "Erwin Frederik Hagen",
          "lat": "56.032162",
          "lng": "9.389174"
      },
      {
          "id": 69,
          "name": "Henrik Volhøj Kristensen",
          "lat": "57.151739",
          "lng": "10.325089"
      },
      {
          "id": 70,
          "name": "Kim Lindgren",
          "lat": "55.22397",
          "lng": "11.938429"
      },
      {
          "id": 71,
          "name": "René Didriksen",
          "lat": "55.654008",
          "lng": "11.7168119"
      },
      {
          "id": 72,
          "name": "Ove Rudbeck",
          "lat": "55.3474483",
          "lng": "8.7502985"
      },
      {
          "id": 73,
          "name": "Olaf Grøn Lyngby",
          "lat": "55.817679",
          "lng": "8.8897702"
      },
      {
          "id": 74,
          "name": "Henry Bertelsen",
          "lat": "55.701175",
          "lng": "8.762219"
      },
      {
          "id": 75,
          "name": "Rene Larsen",
          "lat": "55.6456226",
          "lng": "11.2287574"
      },
      {
          "id": 76,
          "name": "Bjarne Christiansen",
          "lat": "55.6414127",
          "lng": "8.2824925"
      },
      {
          "id": 78,
          "name": "Michael Simonsen",
          "lat": "56.344146",
          "lng": "10.401623"
      },
      {
          "id": 79,
          "name": "Vagn Jensen",
          "lat": "56.559732",
          "lng": "9.4150569"
      },
      {
          "id": 80,
          "name": "Freddy Jensen",
          "lat": "55.4512386",
          "lng": "9.2575201"
      },
      {
          "id": 81,
          "name": "Kim Schou Jørgensen",
          "lat": "57.433041",
          "lng": "10.2689109"
      },
      {
          "id": 82,
          "name": "Mads Flinterup",
          "lat": "56.4573476",
          "lng": "10.4284538"
      },
      {
          "id": 83,
          "name": "Gunnar Johansen",
          "lat": "55.5241788",
          "lng": "11.9820462"
      },
      {
          "id": 84,
          "name": "Jørgen Olsen",
          "lat": "56.149423",
          "lng": "9.499185"
      },
      {
          "id": 85,
          "name": "Morten Friis Jensen",
          "lat": "56.189295",
          "lng": "8.814589"
      },
      {
          "id": 86,
          "name": "Alan Hansen",
          "lat": "56.405046",
          "lng": "10.626352"
      },
      {
          "id": 87,
          "name": "Henrik Jesper Nielsen",
          "lat": "55.4415901",
          "lng": "10.3593899"
      },
      {
          "id": 88,
          "name": "Tommy K. Kristiansen",
          "lat": "55.790146",
          "lng": "9.102763"
      },
      {
          "id": 89,
          "name": "Brian Vels",
          "lat": "56.559501",
          "lng": "9.697582"
      },
      {
          "id": 91,
          "name": "Jim Schou Larsen",
          "lat": "55.976076",
          "lng": "12.335347"
      },
      {
          "id": 92,
          "name": "Peter Knudsen",
          "lat": "55.8741839",
          "lng": "12.3837194"
      },
      {
          "id": 93,
          "name": "Jens Rasmussen",
          "lat": "55.023748",
          "lng": "11.98674"
      },
      {
          "id": 94,
          "name": "Brian K. Jensen",
          "lat": "56.295372",
          "lng": "8.188462"
      },
      {
          "id": 95,
          "name": "Jørgen K. Duus",
          "lat": "55.3011942",
          "lng": "9.3765309"
      },
      {
          "id": 96,
          "name": "Jørgen S. Larsen",
          "lat": "57.26519",
          "lng": "11.069042"
      },
      {
          "id": 97,
          "name": "Jan Nedza",
          "lat": "57.0049816",
          "lng": "10.106309"
      },
      {
          "id": 98,
          "name": "Allan Skriver",
          "lat": "55.053425",
          "lng": "8.943724"
      },
      {
          "id": 100,
          "name": "Claus Bruun Jørgensen",
          "lat": "54.911004",
          "lng": "9.25576"
      },
      {
          "id": 101,
          "name": "Thomas Nielsen",
          "lat": "56.179315",
          "lng": "9.7806409"
      },
      {
          "id": 102,
          "name": "Bent Hanning Kristensen",
          "lat": "56.107092",
          "lng": "9.214161"
      },
      {
          "id": 103,
          "name": "Søren Povlsen Kibsgaard",
          "lat": "56.8495225",
          "lng": "8.709006"
      },
      {
          "id": 104,
          "name": "Per Martin Sass Matthiassen",
          "lat": "56.773688",
          "lng": "9.5157209"
      },
      {
          "id": 105,
          "name": "Tom Gerral Nielsen",
          "lat": "55.867479",
          "lng": "12.330341"
      },
      {
          "id": 106,
          "name": "Niels Guldager",
          "lat": "57.2907756",
          "lng": "9.6600465"
      },
      {
          "id": 107,
          "name": "Asger Kjærsgård",
          "lat": "56.048323",
          "lng": "8.756797"
      },
      {
          "id": 108,
          "name": "Dan Foglmann",
          "lat": "55.9178528",
          "lng": "11.5508825"
      },
      {
          "id": 109,
          "name": "Mads Wrang Rasmussen",
          "lat": "55.112623",
          "lng": "10.549141"
      },
      {
          "id": 110,
          "name": "Henrik Hedegaard",
          "lat": "56.3443253",
          "lng": "8.339356"
      },
      {
          "id": 111,
          "name": "Per Clausen",
          "lat": "55.781445",
          "lng": "12.270153"
      },
      {
          "id": 113,
          "name": "Niels Grønbæk",
          "lat": "56.074186",
          "lng": "9.01407"
      },
      {
          "id": 114,
          "name": "Brian Pape",
          "lat": "56.4746066",
          "lng": "10.5438781"
      },
      {
          "id": 115,
          "name": "Preben Jørgensen",
          "lat": "55.967455",
          "lng": "9.266593"
      },
      {
          "id": 117,
          "name": "Morten Kyrsting Hørup",
          "lat": "55.795117",
          "lng": "12.533856"
      },
      {
          "id": 118,
          "name": "Torsten Blum Hansen",
          "lat": "55.4848404",
          "lng": "11.5592608"
      },
      {
          "id": 119,
          "name": "Henrik Veng Christiansen",
          "lat": "55.60816",
          "lng": "9.757605"
      },
      {
          "id": 120,
          "name": "Bjarne Jørgensen",
          "lat": "54.9526301",
          "lng": "9.567304"
      },
      {
          "id": 121,
          "name": "Claus Aagaard",
          "lat": "54.9495288",
          "lng": "10.8002875"
      },
      {
          "id": 122,
          "name": "Kim Bennedsen",
          "lat": "55.540286",
          "lng": "9.7713059"
      },
      {
          "id": 123,
          "name": "Finn Hyldahl Petersen",
          "lat": "55.7068378",
          "lng": "8.7520536"
      },
      {
          "id": 124,
          "name": "Jens Brandt Andersen",
          "lat": "57.217282",
          "lng": "9.563438"
      },
      {
          "id": 125,
          "name": "Henrik Bendixen",
          "lat": "55.8532659",
          "lng": "11.5611226"
      },
      {
          "id": 127,
          "name": "Benny Frænde Jensen",
          "lat": "57.591339",
          "lng": "10.420284"
      },
      {
          "id": 128,
          "name": "Jens Toft Nielsen",
          "lat": "55.188593",
          "lng": "10.443654"
      },
      {
          "id": 130,
          "name": "Per Ryberg Jensen",
          "lat": "55.504445",
          "lng": "11.883461"
      },
      {
          "id": 131,
          "name": "Søren Gejl",
          "lat": "55.6075188",
          "lng": "8.4878481"
      },
      {
          "id": 132,
          "name": "Ole Brandt",
          "lat": "56.847767",
          "lng": "8.6972536"
      },
      {
          "id": 133,
          "name": "Peter Sørensen",
          "lat": "56.75273",
          "lng": "8.428993"
      },
      {
          "id": 134,
          "name": "Kenneth Jensen",
          "lat": "56.4818742",
          "lng": "9.1429677"
      },
      {
          "id": 135,
          "name": "Carl-Emil Poulsen",
          "lat": "56.398464",
          "lng": "10.353811"
      },
      {
          "id": 136,
          "name": "Christian Christiansen",
          "lat": "56.316975",
          "lng": "9.475017"
      },
      {
          "id": 137,
          "name": "Jens Holm Christensen",
          "lat": "55.693345",
          "lng": "8.992457"
      },
      {
          "id": 138,
          "name": "Ove Stephansen",
          "lat": "56.1119959",
          "lng": "10.1704236"
      },
      {
          "id": 139,
          "name": "Michael Sørensen",
          "lat": "54.804719",
          "lng": "10.714776"
      },
      {
          "id": 140,
          "name": "Henrik Herløv Hansen",
          "lat": "55.442903",
          "lng": "12.152521"
      },
      {
          "id": 141,
          "name": "Niels Henrik Nielsen",
          "lat": "55.4111274",
          "lng": "11.5259553"
      },
      {
          "id": 142,
          "name": "Jesper Rønsholt Jensen",
          "lat": "55.535198",
          "lng": "11.713967"
      },
      {
          "id": 143,
          "name": "Johnny Bastian",
          "lat": "55.6380582",
          "lng": "11.5235603"
      },
      {
          "id": 144,
          "name": "Leif Nielsen",
          "lat": "55.889594",
          "lng": "11.663585"
      },
      {
          "id": 145,
          "name": "Jørgen Normann Christiansen",
          "lat": "54.912256",
          "lng": "11.25522"
      },
      {
          "id": 146,
          "name": "Henning Brusgaard Gebauer",
          "lat": "55.7125607",
          "lng": "12.2510158"
      },
      {
          "id": 147,
          "name": "Jan Olsen",
          "lat": "55.120693",
          "lng": "14.70934"
      },
      {
          "id": 148,
          "name": "Kay Viggo Kræfting",
          "lat": "56.772716",
          "lng": "9.331046"
      },
      {
          "id": 149,
          "name": "Frederik Svendsen",
          "lat": "56.754969",
          "lng": "9.8628509"
      },
      {
          "id": 150,
          "name": "Jens Christian Hansen",
          "lat": "55.948048",
          "lng": "8.463405"
      },
      {
          "id": 151,
          "name": "Frank Lehbert Sørensen",
          "lat": "56.261596",
          "lng": "8.643155"
      },
      {
          "id": 152,
          "name": "Flemming Breinholt Christensen",
          "lat": "56.4837095",
          "lng": "8.5677333"
      },
      {
          "id": 153,
          "name": "Per Kristensen",
          "lat": "55.933611",
          "lng": "9.618554"
      },
      {
          "id": 154,
          "name": "Ib Lund",
          "lat": "54.688228",
          "lng": "11.5273109"
      },
      {
          "id": 155,
          "name": "Henrik Carstensen",
          "lat": "57.101098",
          "lng": "9.577735"
      },
      {
          "id": 156,
          "name": "Jan Niemann Jensen",
          "lat": "57.460521",
          "lng": "10.520729"
      },
      {
          "id": 157,
          "name": "Michael S. Laursen",
          "lat": "56.544801",
          "lng": "8.988564"
      },
      {
          "id": 158,
          "name": "Per Skov",
          "lat": "56.7571853",
          "lng": "8.8760303"
      },
      {
          "id": 159,
          "name": "Preben Herskind Mulbjerg",
          "lat": "56.476998",
          "lng": "10.876446"
      },
      {
          "id": 160,
          "name": "Henrik Skovgaard",
          "lat": "55.444865",
          "lng": "8.389489"
      },
      {
          "id": 161,
          "name": "Kaspar Kristensen",
          "lat": "55.81597",
          "lng": "8.192828"
      },
      {
          "id": 162,
          "name": "Søren Tarp",
          "lat": "55.676758",
          "lng": "8.5807292"
      },
      {
          "id": 163,
          "name": "Heine Tirsbæk Jørgensen",
          "lat": "55.867151",
          "lng": "8.941418"
      },
      {
          "id": 164,
          "name": "Karl Venborg Lauritsen",
          "lat": "55.607835",
          "lng": "9.323928"
      },
      {
          "id": 165,
          "name": "Martin Steiner",
          "lat": "56.2028182",
          "lng": "9.8725668"
      },
      {
          "id": 166,
          "name": "Peter Holm Kristensen",
          "lat": "56.088805",
          "lng": "10.148021"
      },
      {
          "id": 167,
          "name": "Bennet Onsvig",
          "lat": "55.209706",
          "lng": "11.782348"
      },
      {
          "id": 168,
          "name": "Jan Sørensen",
          "lat": "56.3564809",
          "lng": "9.6254927"
      },
      {
          "id": 169,
          "name": "Elon Godiksen",
          "lat": "56.841985",
          "lng": "9.573"
      },
      {
          "id": 170,
          "name": "Hans Nørgaard Bank",
          "lat": "55.9407439",
          "lng": "9.1538097"
      },
      {
          "id": 171,
          "name": "Claus Skovgaard Sørensen",
          "lat": "56.13967",
          "lng": "9.418131"
      },
      {
          "id": 172,
          "name": "Morten Ruggaard",
          "lat": "55.831847",
          "lng": "10.602648"
      },
      {
          "id": 173,
          "name": "Jon Schwarz Sørensen",
          "lat": "55.941991",
          "lng": "10.151824"
      },
      {
          "id": 174,
          "name": "Sophia van Binsbergen",
          "lat": "56.032162",
          "lng": "9.389174"
      },
      {
          "id": 175,
          "name": "Gert Nytoft Jensen",
          "lat": "55.91444",
          "lng": "9.776558"
      },
      {
          "id": 176,
          "name": "Belinda Kristoffersen",
          "lat": "55.0622744",
          "lng": "9.7317584"
      },
      {
          "id": 177,
          "name": "Tom Keinicke",
          "lat": "55.4907087",
          "lng": "11.3240501"
      },
      {
          "id": 178,
          "name": "Martin Vagn Wiberg",
          "lat": "55.9819142",
          "lng": "12.0985651"
      },
      {
          "id": 179,
          "name": "Gunnar Steen Nielsen",
          "lat": "56.688017",
          "lng": "9.728118"
      },
      {
          "id": 180,
          "name": "Jens Waldorff-Hald",
          "lat": "56.934236",
          "lng": "9.757788"
      },
      {
          "id": 181,
          "name": "Martin Rytter Pedersen",
          "lat": "56.464849",
          "lng": "10.330268"
      },
      {
          "id": 182,
          "name": "Leif Qvistgård Pedersen",
          "lat": "56.416186",
          "lng": "8.537941"
      },
      {
          "id": 183,
          "name": "Christian Klit Holm",
          "lat": "57.1384405",
          "lng": "9.3574321"
      },
      {
          "id": 184,
          "name": "Helle Susanne Jensen",
          "lat": "57.460521",
          "lng": "10.520729"
      },
      {
          "id": 185,
          "name": "Mads Winther Jensen",
          "lat": "57.40837",
          "lng": "10.261034"
      },
      {
          "id": 186,
          "name": "Christian Bjarne Thorsen",
          "lat": "57.303148",
          "lng": "11.11955"
      },
      {
          "id": 187,
          "name": "Peter Ortmann Knudsen",
          "lat": "56.292781",
          "lng": "9.1473395"
      },
      {
          "id": 188,
          "name": "Peter Larsen",
          "lat": "56.270931",
          "lng": "9.977215"
      },
      {
          "id": 189,
          "name": "Ole Fogh Petersen",
          "lat": "55.224991",
          "lng": "9.3867972"
      },
      {
          "id": 190,
          "name": "Kim Vig Andersen",
          "lat": "55.6442879",
          "lng": "11.2379069"
      },
      {
          "id": 191,
          "name": "Lars Jens Høgh Jensen",
          "lat": "56.612269",
          "lng": "9.289480"
      },
      {
          "id": 192,
          "name": "Mogens Gade Madsen",
          "lat": "56.168426",
          "lng": "8.356756"
      },
      {
          "id": 193,
          "name": "Lars Møller Christensen",
          "lat": "55.218055",
          "lng": "12.143599"
      },
      {
          "id": 194,
          "name": "Bjarne Christensen",
          "lat": "57.305375",
          "lng": "10.077871"
      },
      {
          "id": 195,
          "name": "Jan Buch Jacobsen",
          "lat": "56.190504",
          "lng": "9.817704"
      },
      {
          "id": 196,
          "name": "Kurt Lynggaard Nielsen",
          "lat": "55.561333",
          "lng": "9.091654"
      },
      {
          "id": 197,
          "name": "Tobias Lygtved Jensen",
          "lat": "57.197499",
          "lng": "9.662723"
      },
      {
          "id": 198,
          "name": "Henriette Vincents",
          "lat": "56.380094",
          "lng": "9.602853"
      },
      {
          "id": 199,
          "name": "Jesper Bentsen",
          "lat": "56.484014",
          "lng": "9.954507"
      },
      {
          "id": 200,
          "name": "Rasmus Rousing Andersen",
          "lat": "56.681912",
          "lng": "10.072426"
      },
      {
          "id": 201,
          "name": "Jens Kurup",
          "lat": "56.231386",
          "lng": "10.290922"
      },
      {
          "id": 202,
          "name": "Jørn Ruben Bøgen",
          "lat": "55.321977",
          "lng": "8.977435"
      },
      {
          "id": 203,
          "name": "Klaus Thusgaard Andersen",
          "lat": "55.945468",
          "lng": "9.3618539"
      }
    ]
  }

}
