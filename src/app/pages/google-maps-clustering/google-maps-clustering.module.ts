import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GoogleMapsClusteringPage } from './google-maps-clustering.page';

const routes: Routes = [
  {
    path: '',
    component: GoogleMapsClusteringPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GoogleMapsClusteringPage]
})
export class GoogleMapsClusteringPageModule {}
