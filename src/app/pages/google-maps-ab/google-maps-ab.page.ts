import { Component, OnInit, NgZone } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Platform, ToastController, PopoverController } from '@ionic/angular';

import {
  GoogleMaps,
  GoogleMapOptions,
  GoogleMapsMapTypeId,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  Circle,
  Spherical,
  GoogleMapsAnimation,
  MyLocation,
  LatLng
} from '@ionic-native/google-maps/ngx';

import { GoogleMapsAbOptionsPage } from '../google-maps-ab-options/google-maps-ab-options.page';

@Component({
  selector: 'app-google-maps-ab',
  templateUrl: './google-maps-ab.page.html',
  styleUrls: ['./google-maps-ab.page.scss'],
  providers: [Spherical]
})
export class GoogleMapsAbPage implements OnInit {

  map: GoogleMap;
  markerA: Marker;
  markerB: Marker;

  public googleForm: FormGroup;
  public distance: Number = 0;
  public showRangeSelection: boolean = false;

  private selCircle: string = '0';
  private numCircle: Number = 5;
  private circles: Circle[] = [];

  constructor(
    private platform: Platform,
    private zone: NgZone,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private popoverCtrl: PopoverController
  ) { 
    this.googleForm = this.formBuilder.group({
      selCircle: [''],
      numCircle: ['']
    });
  }

  async ngOnInit() {
    await this.platform.ready()
    await this.loadMap();
  }

  loadMap() {
    let mapOptions: GoogleMapOptions = {
      mapType: GoogleMapsMapTypeId.SATELLITE,
    
      controls: {
        compass: false,
        myLocationButton: true,
        myLocation: true, // (blue dot)
        zoom: true,       // android only
        mapToolbar: false  // android only
      }
    }
    this.map = GoogleMaps.create('map', mapOptions);
    
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.showToast('Google Map is ready');
      console.log('Google Map ready');
    });
    
    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((position) => {
      this.addMarkerB(position[0]);
    });

    this.goToMyLocation();
  }

  private goToMyLocation(): void {
    this.map.clear();
 
    this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
    
      // Get the location of you
      this.map.getMyLocation().then((location: MyLocation) => {
        
        // Move the map camera to the location with animation
        this.map.animateCamera({
          target: location.latLng,
          zoom: 15,
          // duration: 10000
        });
    
        this.addMarkerA(location.latLng);

      })
      .catch(err => {
        console.error(err.error_message);
        this.showToast(err.error_message);
      });
    });
  }

  private addMarkerA(position: LatLng): void {
    if (!this.markerA) {

      //add marker A
      this.markerA = this.map.addMarkerSync({
        title: 'Marker A',
        position: position,
        animation: GoogleMapsAnimation.BOUNCE,
        draggable: true,
        icon: 'blue'
      });

      this.markerA.on(GoogleMapsEvent.MARKER_DRAG).subscribe((position) => {
        this.clearCircles();
      });

      // if marker is moved, update marker position and calculate distance
      this.markerA.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe((position) => {
        this.markerA.setPosition(position[0]);
        if (this.markerB && this.markerB.isVisible()) {
          this.calcDistance();
        }
        this.updateCircles();
      });
    }
  }

  private addMarkerB(position: LatLng): void {
    if (!this.markerB) {

      //add marker B
      this.markerB = this.map.addMarkerSync({
        title: 'Marker B',
        position: position,
        animation: GoogleMapsAnimation.BOUNCE,
        draggable: true,
        // icon: 'green'
        icon: {
          url: './assets/image/map_marker_crosshair_orange.png'
          // url: 'http://icons.iconarchive.com/icons/iconarchive/red-orb-alphabet/24/Number-1-icon.png'
          // anchor: [16, 16],      
          // size: {
          //   width: 32,
          //   height: 32
          // }
        }
      });
      
      // calculate the distance between A and B
      this.calcDistance();

      // if marker is moved, update marker position and calculate distance
      this.markerB.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe((position) => {
        this.markerB.setPosition(position[0]);
        this.calcDistance();
      });
    }
  }

  private calcDistance(): void {
    this.distance = Math.trunc(Spherical.computeDistanceBetween(this.markerA.getPosition(), this.markerB.getPosition()));
    // console.log(`Distance approx. ${this.distance} meters`);
    // this.showToast(`Distance approx. ${this.distance} meters`);
    this.forceScreenRefresh();
  }

  private clearCircles(): void {
    // remove circles if any
    if (this.circles.length > 0) {
      for (let c = 0; c < this.circles.length; c++) {
        this.circles[c].remove();
      }
      this.circles = [];
    }
  }

  private updateCircles(): void {
    if (this.markerA && this.markerA.isVisible()) {
      this.clearCircles();

      if (parseInt(this.selCircle) != 0) {
        let position = this.markerA.getPosition();
      
        for (let c = 0; c < this.numCircle; c++) {
          let radius = (c+1) * parseInt(this.selCircle);
  
          let circle: Circle = this.map.addCircleSync({
            center: position,
            radius: radius,
            strokeColor: '#F5F5F5',
            strokeWidth: 1,
            fillColor: 'rgba(0, 0, 255, 0.0)',
            clickable : false
          });
  
          // circle.on(GoogleMapsEvent.CIRCLE_CLICK).subscribe((params: any[]) => {
          //   console.log('circle click data: ', params);
          // });

          this.circles.push(circle);
        }
      }
    }
  }

  public async btnOptions(ev: Event) {
    const options = await this.popoverCtrl.create({
      component: GoogleMapsAbOptionsPage,
      componentProps: {
        range: this.selCircle
      },
      event: ev
    });

    options.onDidDismiss().then((data) => {
      if (data['data']) {
        this.selCircle = data['data'].value; // selected range
        
        this.updateCircles();
      }
    });

    return await options.present();
  }

  private forceScreenRefresh(): void {
    this.zone.run(() => {
      // console.log('force screen update!');
    });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2500,
      position: 'middle'
    });
    toast.present();
  }

}
